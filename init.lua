minetest.register_privilege("worldedit", {
    description = "Use cg-lab4 commands",
})

minetest.register_chatcommand("fill", {
	privs = {
		worldedit = true,
	},
	func = function(name, param)
		local parts = param:split(" ")
		local name = parts[1]
		if not minetest.registered_nodes[name] then
			return false
		end
		local x1 = tonumber(parts[2])
		local y1 = tonumber(parts[3])
		local z1 = tonumber(parts[4])
		local x2 = tonumber(parts[5])
		local y2 = tonumber(parts[6])
		local z2 = tonumber(parts[7])
		if x1 == nil or y1 == nil or z1 == nil
			or x2 == nil or y2 == nil or z2 == nil then
			return false
		end
		x1 = math.floor(x1 + 0.5)
		y1 = math.floor(y1 + 0.5)
		z1 = math.floor(z1 + 0.5)
		x2 = math.floor(x2 + 0.5)
		y2 = math.floor(y2 + 0.5)
		z2 = math.floor(z2 + 0.5)
		local dx,dy,dz = 1,1,1
		if x1 > x2 then dx=-1 end
		if y1 > y2 then dy=-1 end
		if z1 > z2 then dz=-1 end
		for i = x1,x2,dx do
		for j = y1,y2,dy do
		for k = z1,z2,dz do
			minetest.set_node({x=i,y=j,z=k}, {name = name})
		end end end
		return true, "filled " .. name .. " from "
			.. x1 .. " " ..  y1 .. " " .. z1 .. " to "
			.. x2 .. " " .. y2 .. " " .. z2
	end,
})

minetest.register_chatcommand("line", {
	privs = {
		worldedit = true,
	},
	func = function(name, param)
		local parts = param:split(" ")
		local name = parts[1]
		if not minetest.registered_nodes[name] then
			return false
		end
		local x1 = tonumber(parts[2])
		local y1 = tonumber(parts[3])
		local z1 = tonumber(parts[4])
		local x2 = tonumber(parts[5])
		local y2 = tonumber(parts[6])
		local z2 = tonumber(parts[7])
		if x1 == nil or y1 == nil or z1 == nil
			or x2 == nil or y2 == nil or z2 == nil then
			return false
		end

		local message = "line of " .. name .. " from "
			.. x1 .. " " ..  y1 .. " " .. z1 .. " to "
			.. x2 .. " " .. y2 .. " " .. z2

		x1 = math.floor(x1 + 0.5)
		y1 = math.floor(y1 + 0.5)
		z1 = math.floor(z1 + 0.5)
		x2 = math.floor(x2 + 0.5)
		y2 = math.floor(y2 + 0.5)
		z2 = math.floor(z2 + 0.5)

		local dx = math.abs(x2 - x1)
		local dy = math.abs(y2 - y1)
		local dz = math.abs(z2 - z1)
		local xs,ys,zs = 1,1,1
		if x1 > x2 then xs = -1 end
		if y1 > y2 then ys = -1 end
		if z1 > z2 then zs = -1 end

		local p1, p2
		if dx >= dy and dx >= dz then
			p1 = 2 * dy - dx
			p2 = 2 * dz - dx
			while x1 ~= x2 do
				x1 = x1 + xs
				if p1 >= 0 then
					y1 = y1 + ys
					p1 = p1 - 2 * dx
				end
				if p2 >= 0 then
					z1 = z1 + zs
					p2 = p2 - 2 * dx
				end
				p1 = p1 + 2 * dy
				p2 = p2 + 2 * dz
				minetest.set_node({x=x1,y=y1,z=z1}, {name = name})
			end
		elseif dy >= dx and dy >= dz then
			p1 = 2 * dx - dy
			p2 = 2 * dz - dy
			while y1 ~= y2 do
				y1 = y1 + ys
				if p1 >= 0 then
					x1 = x1 + xs
					p1 = p1 - 2 * dy
				end
				if p2 >= 0 then
					z1 = z1 + zs
					p2 = p2 - 2 * dy
				end
				p1 = p1 + 2 * dx
				p2 = p2 + 2 * dz
				minetest.set_node({x=x1,y=y1,z=z1}, {name = name})
			end
		else
			p1 = 2 * dy - dz
			p2 = 2 * dx - dz
			while z1 ~= z2 do
				z1 = z1 + zs
				if p1 >= 0 then
					y1 = y1 + ys
					p1 = p1 - 2 * dz
				end
				if p2 >= 0 then
					x1 = x1 + xs
					p2 = p2 - 2 * dz
				end
				p1 = p1 + 2 * dy
				p2 = p2 + 2 * dx
				minetest.set_node({x=x1,y=y1,z=z1}, {name = name})
			end
		end

		return true, message
	end,
})

